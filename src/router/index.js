import Vue from 'vue'
import VueRouter from 'vue-router'
//import HomeView from '../views/HomeView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('../Modulos/Home/views/Home.vue')
  },
  {
    path: '/principalV',
    name: 'principalV',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../Modulos/Principal/views/principalV.vue')
  },
  {
    path: '/clienteV',
    name: 'clienteV',
    
    component: () => import( '../Modulos/Clientes/Views/clienteV.vue')
  },
  {
    path: '/indumentariaV',
    name: 'indumentariaV',
    
    component: () => import( '../Modulos/Indumentaria/Views/indumentariaV.vue')
  },
  {
    path: '/eventosV',
    name: 'eventosV',
    
    component: () => import( '../Modulos/Eventos/Views/eventosV.vue')
  },
  {
    path: '/contactoV',
    name: 'contactoV',
    
    component: () => import( '../Modulos/Contacto/Views/contactoV.vue')
  },
  {
    path: '/tallesV',
    name: 'tallesV',
    
    component: () => import( '../Modulos/Indumentaria/Talles/Views/tallesV.vue')
  },
  {
    path: '/tiendaV',
    name: 'tiendaV',
    
    component: () => import( '../Modulos/Indumentaria/Tienda/Views/tiendaV.vue')
  },
  {
    path: '/compraV',
    name: 'compraV',
    
    component: () => import( '../Modulos/Indumentaria/Tienda/Compra/Components/compra.vue')
  },
  {
    path: '/re001V',
    name: 're001V',
    
    component: () => import( '../Modulos/Indumentaria/Tienda/Compra/RE001/Views/re001V.vue')
  },
  {
    path: '/rfd001V',
    name: 'rfd001V',
    
    component: () => import( '../Modulos/Indumentaria/Tienda/Compra/RFD001/Views/rfd001V.vue')
  },
  {
    path: '/atfutsV',
    name: 'atfutsV',
    
    component: () => import( '../Modulos/Indumentaria/Tienda/Compra/ATFUTS/Views/atfutsV.vue')
  },
  {
    path: '/casmfsV',
    name: 'casmfsV',
    
    component: () => import( '../Modulos/Indumentaria/Tienda/Compra/CASMFS/Views/casmfsV.vue')
  },
  {
    path: '/CarritoV',
    name: 'carritoV',
    
    component: () => import( '../Modulos/Carrito/Views/carritoV.vue'),

    props: true
  }

  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
